﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Food 
{
	public string name;
	public string description;
	public Sprite image;
	public float happiness;
	public float fullness;
	public int coin;
	public int gem;
	public int exp;
}
