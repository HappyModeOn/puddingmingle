﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using UnityEngine.UI;


public class GameManager : MonoBehaviour 
{
	public TextureManager textureManager;
	public GameObject PuddingPrefab;
	public List<Pudding> activePuddings;
	public List<Food> foodList;
	private int totalPuddingInField = 10;
	public Camera cam;
	private Vector3 originalCamPosition;

    public int coin = 0;
    public Text coin_text;
    public int gem = 0;
    public Text gem_text;
    public float magic_powder = 0;
    public float magic_powder_max = 1;
    public Slider magic_powder_slider;

    public int food = 50;
    public Text food_count;
    public GameObject foodUI;
    public GameObject hatcherUI;
    public GameObject closeMenu;
    public GameObject shopMenu;
    public GameObject pediaMenu;
    public GameObject optionMenu;
    public GameObject foodMenu;
    public GameObject partyMenu;
    public Button foodButton;
    public Button menuButton;
    public Button foodCloseButton;

    public Button purchaseFoodButtonCoin;
    public Button purchaseFoodButtonAd;
    public Button purchaseFoodButtonGem;

    private int unlock_hatcher = 0;// 1 - 4
    public GameObject[] locked;

    private float magic_divider = 1;//*******///
    /// <summary>
    /// 0 = hatcher
    /// 1 = food menu
    /// 2 = party
    /// </summary>
    public static int currentMenuID = 0;
    void Start()
	{
        SyncData();

        foodButton.onClick.AddListener(FoodUI);
        foodCloseButton.onClick.AddListener(CloseFoodUI);

        purchaseFoodButtonCoin.onClick.AddListener(PurchaseFoodCoin);
        purchaseFoodButtonAd.onClick.AddListener(PurchaseFoodAd);
        purchaseFoodButtonGem.onClick.AddListener(PurchaseFoodGem);
		originalCamPosition = cam.transform.position;
		CreatePudding();
		ListingFood();
        UpdateCurrency();
        initMagicPowder();
    }

    void FoodUI()
    {
        currentMenuID = 1;
        foreach (Pudding item in activePuddings)
        {
            item.ToggleCollider(false);
        }
        foodUI.SetActive(true);
        hatcherUI.SetActive(false);
        closeMenu.SetActive(true);
    }

    void CloseFoodUI()
    {
        readyForFeed = false;
        feeding = false;
        currentMenuID = 0;
        foodUI.SetActive(false);
        hatcherUI.SetActive(true);
        closeMenu.SetActive(false);
    }

    void PurchaseFoodCoin()
    {
        if (coin >= 50)
        {
            coin -= 50;
            food += 100;
        }
        UpdateCurrency();
    }

    void PurchaseFoodAd()
    {
        ShowRewardedAd();
    }

    void PurchaseFoodGem()
    {
        if (gem >= 1)
        {
            gem -= 1;
            food += 1200;
        }
        UpdateCurrency();
    }

    void SyncData()
    {
        Load();
        Debug.Log("Syncing");
    }

    void OnApplicationQuit()
    {
        Save();
    }

    void Save()
    {
        PlayerPrefs.SetInt("COIN", coin);
        PlayerPrefs.SetInt("GEM", gem);
        PlayerPrefs.SetFloat("MP", magic_powder);
        PlayerPrefs.SetFloat("MPD", magic_divider);
        // puddings
        // pedia + Player level
        PlayerPrefs.SetInt("FOOD", food);
        PlayerPrefs.Save();
        // 
    }

    void Load()
    {
        coin = PlayerPrefs.GetInt("COIN", 50);
        gem = PlayerPrefs.GetInt("GEM", 0);
        magic_powder = PlayerPrefs.GetFloat("MP", 0);
        magic_divider = PlayerPrefs.GetFloat("MPD", 1);
        food = PlayerPrefs.GetInt("FOOD", 100);
    }

    void initMagicPowder()
    {
        //magic_powder_slider.onValueChanged.AddListener(UpdateMagicPowder);
        magic_powder_slider.value = magic_powder;
        magic_powder_slider.maxValue = magic_powder_max;
    }

    void UpdateMagicPowder(float value)
    {
        magic_powder_slider.value = value;
    }

    void UpdateCurrency()
    {
        coin_text.text = coin.ToString();
        gem_text.text = gem.ToString();
        food_count.text = "X " + food.ToString();
    }

		void ListingFood()
		{
				foodList = new List<Food>(textureManager.foodSprite.Count);
				for (int i = 0; i < textureManager.foodSprite.Count;i++)
				{
						Food tempFood = new Food();
						tempFood.image= textureManager.foodSprite[i];
						foodList.Add( tempFood);
				}
		}
	void CreatePudding () 
	{
				float x = -6;
				float y = 0;
				activePuddings = new List<Pudding>();
				for (int i = 0; i < totalPuddingInField;i++)
				{
						GameObject tempObj = Instantiate(PuddingPrefab, new Vector3(x,y,0), Quaternion.identity) as GameObject;
						tempObj.name = i.ToString();

                        activePuddings.Add(tempObj.GetComponent<Pudding>());

						x += 2.5f;
					//activePuddings[i].Paint(i);
				}

				for (int i = 0;i < activePuddings.Count;i++)
				{
						activePuddings[i].Init();
						bool tmpBaby = false;
						if (Random.Range(0, 50) < 25)
						{
								tmpBaby = true;
						}
						activePuddings[i].Paint(i, tmpBaby);
				}


	}
	void Update()
	{
        magic_powder_slider.value = magic_powder;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        //Hack
        if (Input.GetKeyDown(KeyCode.Return))
        {
            coin = 9999;
            food = 999;
            gem = 99;
        }
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            coin = 50;
            gem = 0;
            food = 100;
            PlayerPrefs.DeleteAll();
            UpdateCurrency();
        }

        // optimize
        if (currentMenuID == 1 && feeding == false)
        {
            if (readyForFeed == false)
            {
                foreach (Pudding item in activePuddings)
                {
                    if (item.reach_feed_pos == false)
                    {
                        return;
                    }
                }
                readyForFeed = true;
            }
            else
            {
                foreach (Pudding item in activePuddings)
                {
                    item.ToggleCollider(true);
                }
                feeding = true;
            }
        }
        
    }
    //
    bool readyForFeed = false;
    bool feeding = false;
    //
    //ADS
    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                food += 200;
                UpdateCurrency();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
