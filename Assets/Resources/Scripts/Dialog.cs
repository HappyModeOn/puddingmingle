﻿using UnityEngine;
using System.Collections;

public class Dialog : MonoBehaviour {

	// Use this for initialization
	void Start () {
		spd = 0.05f;
	}
	
	// Update is called once per frame
	float lifeTime = 0.5f;
	public float spd = 0.5f;
	void Update () {

		if (transform.parent == null)
		{
			lifeTime -= Time.deltaTime;
			transform.Translate(Vector3.up * (Time.deltaTime + spd)/2);
		}
		if (lifeTime<0)
		{
			Destroy(gameObject);
		}
	
	}
}
