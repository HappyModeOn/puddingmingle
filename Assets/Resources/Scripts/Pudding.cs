﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pudding : MonoBehaviour
{
    private int id;
	private TextureManager textureManager;
	private Material bodyShape;
	private Material bodyShade;
	private Material face;
	private Material propShape;
	private Material propShade;
	private Material topping;
	private GameObject toppingGameObject;
	private SpriteRenderer statusEmo;
	private Animator animator;
	public bool isBaby = false;
	public int faceID = 0;

		//private GameObject obj;

		/*
	public int bodyID = 0;//Body And Prop(Ear Tail Wing)
	public int faceID = 0;
	public int colorID = 0;
	public int toppingID = 0;
*/
	public void Init()
	{
        id = int.Parse(gameObject.name);
		idleTime = Random.Range(1, 10);
		animator = gameObject.GetComponent<Animator>();
		textureManager = GameObject.Find("TextureManager").GetComponent<TextureManager>();
		//obj = Resources.Load("PuddingPrefab") as GameObject;
		//Instantiate(obj);
		//------------------------------Init-----------------//

		//animator = obj

		bodyShape = transform.GetChild (0).transform.GetChild (0).transform.GetChild (1).transform.GetChild (0).gameObject.GetComponent<SkinnedMeshRenderer>().materials[0];
		bodyShade = transform.GetChild (0).transform.GetChild (0).transform.GetChild (1).transform.GetChild (0).gameObject.GetComponent<SkinnedMeshRenderer>().materials[1];
		face = transform.GetChild (0).transform.GetChild (0).transform.GetChild (1).transform.GetChild (1).gameObject.GetComponent<SkinnedMeshRenderer> ().materials [0];
		propShape = transform.GetChild (0).transform.GetChild (0).transform.GetChild (1).transform.GetChild (2).gameObject.GetComponent<SkinnedMeshRenderer> ().materials [0];
		propShade = transform.GetChild (0).transform.GetChild (0).transform.GetChild (1).transform.GetChild (2).gameObject.GetComponent<SkinnedMeshRenderer> ().materials [1];

		topping = transform.GetChild (0).transform.GetChild (0).transform.GetChild (1).transform.GetChild (3).GetComponent<SkinnedMeshRenderer> ().materials [0];
		toppingGameObject = transform.GetChild (0).transform.GetChild (0).transform.GetChild (1).transform.GetChild (3).gameObject;
		statusEmo = transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).GetComponent<SpriteRenderer>();
		//-----------------------------End-Init-----------------//

		//Deploy
	}

	public void Paint(int id, bool baby)
	{
		faceID = id;
		isBaby = baby;
		if (baby)
		{
			gameObject.GetComponent<Rigidbody2D>().mass = 1;// Can play with weight
			transform.localScale = new Vector3(0.25f,0.25f,-0.25f);
			bodyShade.mainTexture = textureManager.babyBodyShadeList[id];
			face.mainTexture = textureManager.babyFaceList [id];
			propShape.mainTexture = textureManager.babyPropList [id];
			propShade.mainTexture = textureManager.babyPropShadeList [id];

			toppingGameObject.SetActive(false);
		}
		else
		{
			gameObject.GetComponent<Rigidbody2D>().mass  = 50;// Can play with weight
			transform.localScale = new Vector3(0.5f,0.5f,-0.5f);
			bodyShade.mainTexture = textureManager.bodyShadeList[id];
			face.mainTexture = textureManager.faceList [id];
			propShape.mainTexture = textureManager.propList [id];
			propShade.mainTexture = textureManager.propShadeList [id];

			topping.mainTexture = textureManager.toppingList [id];
		}

		propShape.renderQueue = 2448;
		propShade.renderQueue = 2449;
		bodyShape.renderQueue = 2450;
		bodyShade.renderQueue = 2451;

		bodyShape.color = textureManager.colorList [id];
		bodyShade.color = textureManager.colorList [id];
		propShape.color = textureManager.colorList [id];
		propShade.color = textureManager.colorList[id];
		//-----------------------------End-Paint-----------------//
		StatusUpdate();
	}


		public int animID;
		public bool updateAnim = false;

		public void UpdateFace(int faceNo)
		{
			// -1 is Default Face
			if (faceNo == -1 )
			{
				if (isBaby)
				{
					face.mainTexture = textureManager.babyFaceList [faceID];
				}
				else
				{
					face.mainTexture = textureManager.faceList [faceID];
				}
			}
			else
			{
				face.mainTexture = textureManager.faceEmo[faceNo];
			}

		}


		void PlayAnimation (int animID)
		{
			//face 0 = happy
			//face 4 = Eating
			if (animator.GetBool("Play"))
			{
				animator.SetBool("Play", false);
			}
			else
			{
				animator.SetBool("Play", true);
			}

			updateAnim = false;
		}

    public void ToggleCollider(bool value)
    {
        gameObject.GetComponent<BoxCollider2D>().enabled = value;
        if (value) return;

        reach_feed_pos = false;
        //optimize
        if (id == 0)
        {
            targetPos = new Vector3(-2, 1.95f, targetPos.z);
        }
        else if (id == 1)
        {
            targetPos = new Vector3(-0, 2.38f, targetPos.z);
        }
        else if (id == 2)
        {
            targetPos = new Vector3(2.58f, 1.93f, targetPos.z);
        }
        else if (id == 3)
        {
            targetPos = new Vector3(-3.77f, 0.54f, targetPos.z);
        }
        else if (id == 4)
        {
            targetPos = new Vector3(-1.33f, 0.54f, targetPos.z);
        }
        else if (id == 5)
        {
            targetPos = new Vector3(1.37f, 0.54f, targetPos.z);
        }
        else if (id == 6)
        {
            targetPos = new Vector3(3.8f, 0.54f, targetPos.z);
        }
        else if (id == 7)
        {
            targetPos = new Vector3(-2.58f, -0.83f, targetPos.z);
        }
        else if (id == 8)
        {
            targetPos = new Vector3(0.22f, -1.41f, targetPos.z);
        }
        else if (id == 9)
        {
            targetPos = new Vector3(2.8f, -0.83f, targetPos.z);
        }
        animator.SetBool("Walk", true);
    }

    public bool reach_feed_pos = false;
	void Update ()
	{
        if (GameManager.currentMenuID == 1)
        {
            //gameObject.GetComponent<Rigidbody2D>()
           
            
            // need to remove rigid for a while and idle when reach target
            if (Vector2.Distance(new Vector2(transform.position.x, transform.position.y), new Vector2(targetPos.x, targetPos.y)) <= 0.1f)
            {
                StopWalk();
                Idle();
                reach_feed_pos = true;
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, targetPos, (Time.deltaTime * moveSpeed * 2.5f) / Vector3.Distance(transform.position, targetPos));
            }
           
        }
        else
        {
            if (idleTime > 0)
            {
                idleTime -= Time.deltaTime;
            }
            else
            {
                move = true;
                idleTime = Random.Range(1, 10);
            }



            if (move)
            {
                Walk();
            }
            
        }
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y);
    }
		public float fullness = 0.5f;
		public float happiness = 0.5f;

		void StatusUpdate()
		{
				int status = -1;
				if (happiness < 0.5f)
				{
					status = 0;
				}
				if (fullness < 0.5f)
				{
					status = 1;
				}
				StatusShow(status);
		}
		void StatusShow(int statusID)
		{
				if (statusID < 0)
				{
					StatusHide();
				}
				else
				{
					//0 = bore
					//1 = hungry
					statusEmo.sprite = textureManager.statusEmo[statusID];
					statusEmo.enabled = true;
				}
		}
		void StatusHide()
		{
			statusEmo.enabled = false;
		}

		void Idle()
		{
				animator.SetBool("Walk", false);
		}

		private float idleTime = 5;

		private float moveSpeed = 1f;
		private bool isWalking = false;
		private  bool move = false;
		private Vector3 targetPos;
		private float walkingTimeOut = 5;
		void Walk()
		{
				if (!isWalking)
				{
						animator.SetBool("Walk", true);
						targetPos = new Vector3(Random.Range(-4.5f, 4.5f), Random.Range(-2.7f, 2.7f), transform.position.z);
						isWalking = true;

				}
				if (Vector3.Distance(transform.position, targetPos) < 1)
				{
						StopWalk();
				}
				else
				{
					if (walkingTimeOut > 0)
					{
							walkingTimeOut -= Time.deltaTime;
							transform.position = Vector3.Lerp(transform.position, targetPos, (Time.deltaTime) / Vector3.Distance(transform.position, targetPos) * moveSpeed);
					}
					else
					{
							StopWalk();
					}

				}
		}

		void StopWalk()
		{
				walkingTimeOut = 5;
				move = false;
				isWalking = false;
				animator.SetBool("Walk", false);
		}
		public bool isSelect = false;

	void OnMouseDown()
	{
        if (GameManager.currentMenuID == 1)
        {
            Eating();
        }
        else
        {
            Debug.Log(gameObject.name + "Generate Magic Powder");
        }
        
	}
    public AudioClip eat;
	void Eating()
	{
		animator.SetTrigger("Eat");
        gameObject.GetComponent<AudioSource>().PlayOneShot(eat);
       
	}
}
