﻿using UnityEngine;
using System.Collections;

public class Egg : MonoBehaviour {

	// Use this for initialization
	public AudioClip sfx;
	Transform pivot;
	void Awake () 
	{


		pivot = GameObject.Find("WackoPivotPoint").transform;
		directionR = Vector3.forward;
		timeCount = maxTime/2;
		speed = maxSpeed;

	}


	public int minRan = 0;
	public int maxRan = 0;
	float startIn = 0.5f;
	int count = 0;
	public int breakCount = 3;
	float timeToStop = 0;
	float timeCount = 0;
	public float maxTime = 3;
	float speed = 50;
	public float maxSpeed = 50;
	Vector3 directionR;

	bool isFreezeTime = false;
	public bool isStart = false;
	public bool reloadCG = false;
	// Update is called once per frame
	void Update () {
		if (reloadCG)
		{
			int randEggImg = Random.Range(minRan,maxRan);
			Debug.Log (randEggImg + "<" + minRan + "/" + maxRan + ">");
			if (randEggImg == 0)
			{
				randEggImg++;
			}
			gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Images/Texture/EGG/e-" + randEggImg);
			Debug.Log ("Images/Texture/EGG/e-" + randEggImg);
			reloadCG = false;
		}

		if (isStart)
		{
			if (pivot)
			{
				startIn-= Time.smoothDeltaTime;
				if (breakCount > 0 && startIn < 0)
				{
					if (count == 0)
					{
						transform.RotateAround(pivot.position, directionR, speed * Time.smoothDeltaTime);
						timeCount-= Time.smoothDeltaTime;
						
						if (timeCount < 0)
						{
							GetComponent<AudioSource>().PlayOneShot(sfx);
							count++;
							directionR.z *= -1;
							timeCount = maxTime;
							
						}
						
					}
					else if (count == 1)
					{
						transform.RotateAround(pivot.position, directionR, speed * Time.smoothDeltaTime);
						timeCount-= Time.smoothDeltaTime;
						
						if (timeCount < 0)
						{
							GetComponent<AudioSource>().PlayOneShot(sfx);
							count++;
							directionR.z *= -1;
							timeCount = maxTime/2;
						}
					}
					else if (count == 2)
					{
						transform.RotateAround(pivot.position, directionR, speed * Time.smoothDeltaTime);
						timeCount-= Time.smoothDeltaTime;
						
						if (timeCount < 0)
						{
							//audio.PlayOneShot(sfx);
							count++;
							directionR.z *= -1;
							timeToStop = maxTime/2;
							isFreezeTime = true;
						}
					}
					
					if (isFreezeTime)
					{
						timeToStop -= Time.smoothDeltaTime;
						if (timeToStop < 0)
						{
							//transform.eulerAngles = Vector3.zero;
							count = 0;
							isFreezeTime = false;
							directionR.z *= -1;
							timeCount = maxTime/2;
							breakCount--;
						}
					}
				}
				else if (breakCount < 1)
				{
					Debug.Log ("breakCount");
				}
				
			}
		}



	
	}
}
