﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextureManager : MonoBehaviour 
{
	public List<Color> colorList;

	public List<Texture> bodyShadeList;
	public List<Texture> faceList;
	public List<Texture> propList;
	public List<Texture> propShadeList;

	public List<Texture> babyBodyShadeList;
	public List<Texture> babyFaceList;
	public List<Texture> babyPropList;
	public List<Texture> babyPropShadeList;

	public List<Texture> toppingList;

	public List<Texture> faceEmo;
	public List<Sprite> statusEmo;
	public List<Sprite> foodSprite;
	public bool ready = false;
	// Use this for initialization
	void Awake () 
	{
		ColorInit ();
		TextureInit();
	}

	void ColorInit ()
	{
			colorList = new List<Color> ();
			colorList.Add (new Color (0.69f, 0.69f, 0.69f, 1));
			colorList.Add (new Color (0.95f, 0.92f, 0.83f, 1));
			colorList.Add (new Color (0.35f, 0.69f, 0.87f, 1));
			colorList.Add (new Color (0.89f, 0.9f, 0.89f, 1));
			colorList.Add (new Color (0.94f, 0.89f, 0.44f, 1));
			colorList.Add (new Color (0.96f, 0.85f, 0.73f, 1));
			colorList.Add (new Color (0.99f, 0.85f, 0.85f, 1));
			colorList.Add (new Color (0.85f, 0.85f, 0.83f, 1));
			colorList.Add (new Color (0.68f, 0.66f, 0.65f, 1));
			colorList.Add (new Color (0.96f, 0.92f, 0.75f, 1));
	}

	public int totalPudding = 10;
	public int totalFaceEmo = 6;
	public int totalStatusEmo = 2;
	public int totalFood = 6;

	void TextureInit()
	{
				bodyShadeList = new List<Texture>(totalPudding);
				faceList = new List<Texture>(totalPudding);
				propList = new List<Texture>(totalPudding);
				propShadeList = new List<Texture>(totalPudding);
				babyBodyShadeList = new List<Texture>(totalPudding);
				babyFaceList = new List<Texture>(totalPudding);
				babyPropList = new List<Texture>(totalPudding);
				babyPropShadeList = new List<Texture>(totalPudding);
				toppingList = new List<Texture>(totalPudding);
				faceEmo = new List<Texture>(totalFaceEmo);
				statusEmo = new List<Sprite>(totalStatusEmo);
				foodSprite = new List<Sprite>(totalFood);

				for (int i = 0; i < totalPudding;i++)
				{
						bodyShadeList.Add(Resources.Load("Textures/texture_BodyShade_" + i.ToString()) as Texture);

						faceList.Add(Resources.Load("Textures/texture_Face_" + i.ToString()) as Texture);
						propList.Add(Resources.Load("Textures/texture_PropShape_" + i.ToString()) as Texture);
						propShadeList.Add(Resources.Load("Textures/texture_PropShade_" + i.ToString()) as Texture);

						babyBodyShadeList.Add(Resources.Load("Textures/texture_baby_BodyShade_" + i.ToString()) as Texture);
						babyFaceList.Add(Resources.Load("Textures/texture_baby_Face_" + i.ToString()) as Texture);
						babyPropList.Add(Resources.Load("Textures/texture_baby_PropShape_" + i.ToString()) as Texture);
						babyPropShadeList.Add(Resources.Load("Textures/texture_baby_PropShade_" + i.ToString()) as Texture);

						toppingList.Add(Resources.Load("Textures/texture_Topping_" + i.ToString()) as Texture);
				}

				for (int i = 0; i < totalFaceEmo;i++)
				{
						faceEmo.Add(Resources.Load("Textures/texture_FaceEmo_" + i.ToString()) as Texture);
				}

				for (int i = 0; i < totalStatusEmo;i++)
				{
						statusEmo.Add(Resources.Load<Sprite>("Textures/Popup_Status_" + i.ToString()) as Sprite);
				}

				for (int i = 0; i < totalFood;i++)
				{
						foodSprite.Add(Resources.Load<Sprite>("Textures/Food_" + i.ToString()) as Sprite);
				}
				ready = true;
	}
}
